# Generated by Django 3.2.7 on 2021-12-09 19:14

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_populate_gis_orgs'),
    ]

    operations = [
        migrations.AddField(
            model_name='review',
            name='created_dtm',
            field=models.DateTimeField(default=datetime.datetime(2021, 12, 9, 19, 14, 10, 794309)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='review',
            name='edited_dtm',
            field=models.DateTimeField(default=datetime.datetime(2021, 12, 9, 19, 14, 20, 688438)),
            preserve_default=False,
        ),
    ]
